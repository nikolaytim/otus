<?php

use App\Adapter\DebugAdapter;
use App\Adapter\MoveAdapter;
use App\Adapter\RotateAdapter;
use App\Adapter\ChangeVectorVelocityAdapter;
use App\Adapter\FuelAdapter;
use App\Adapter\CheckFuelAdapter;
use App\Command\ChangeVectorVelocityCommand;
use App\Command\CheckFuelCommand;
use App\Command\DebugSpaceObjectCommand;
use App\Command\ExitGameCommand;
use App\Command\FuelCommand;
use App\Command\MoveCommand;
use App\Command\MoveWithFuelCommand;
use App\Command\RotateCommand;
use App\Command\RotateWithChangeVectorVelocityCommand;
use App\ExceptionHandler\ExceptionHandler;
use App\Model\AbstractSpaceObject;
use App\Model\Direction;
use App\Model\Vector;

require_once dirname(__DIR__) . '/vendor/autoload.php';

$position = new Vector(200, 100);
$direction = new Direction(1, 8);
$velocity = 5;
$spaceObject = new AbstractSpaceObject($position, $direction);
//$spaceObject->setProperty('velocity', $velocity);
$spaceObject->setProperty('quantityFuel', 10);
$spaceObject->setProperty('rateFuel', 3);

$moveAdapter = new MoveAdapter($spaceObject);
$rotateAdapter = new RotateAdapter($spaceObject);
$changeVectorVelocityAdapter = new ChangeVectorVelocityAdapter($spaceObject);
$fuelAdapter = new FuelAdapter($spaceObject);
$checkFuelAdapter = new CheckFuelAdapter($spaceObject);
$debugAdapter = new DebugAdapter($spaceObject);

$exitGameCommand = new ExitGameCommand();
$moveCommand = new MoveCommand($moveAdapter);
$rotateCommand = new RotateCommand($rotateAdapter);
$changeVectorVelocityCommand = new ChangeVectorVelocityCommand($changeVectorVelocityAdapter);
$fuelCommand = new FuelCommand($fuelAdapter);
$checkFuelCommand = new CheckFuelCommand($checkFuelAdapter);
$debugSpaceObjectCommand = new DebugSpaceObjectCommand($debugAdapter);

$moveWithFuelCommand = new MoveWithFuelCommand([
    $checkFuelCommand,
    $moveCommand,
    $fuelCommand
]);

$rotateWithChangeVectorVelocityCommand = new RotateWithChangeVectorVelocityCommand([
    $rotateCommand,
    $changeVectorVelocityCommand,
]);

$queue = new SplQueue();
$queue->setIteratorMode(SplQueue::IT_MODE_DELETE);
/*
$queue->enqueue($rotateWithChangeVectorVelocityCommand);
$queue->enqueue($debugSpaceObjectCommand);
$queue->enqueue($rotateWithChangeVectorVelocityCommand);
$queue->enqueue($debugSpaceObjectCommand);
*/

$queue->enqueue($moveWithFuelCommand);
$queue->enqueue($debugSpaceObjectCommand);
$queue->enqueue($moveWithFuelCommand);
$queue->enqueue($debugSpaceObjectCommand);
$queue->enqueue($moveWithFuelCommand);
$queue->enqueue($debugSpaceObjectCommand);
$queue->enqueue($moveWithFuelCommand);
$queue->enqueue($debugSpaceObjectCommand);
$queue->enqueue($moveWithFuelCommand);
$queue->enqueue($debugSpaceObjectCommand);


/*
$queue->enqueue($moveCommand);
//$queue->enqueue($debugSpaceObjectCommand);

$queue->enqueue($moveCommand);
//$queue->enqueue($debugSpaceObjectCommand);

$queue->enqueue($rotateCommand);
//$queue->enqueue($debugSpaceObjectCommand);

$queue->enqueue($moveCommand);
//$queue->enqueue($debugSpaceObjectCommand);

$queue->enqueue($rotateCommand);
//$queue->enqueue($debugSpaceObjectCommand);

//$queue->enqueue($exitGameCommand);

$queue->enqueue($moveCommand);
//$queue->enqueue($debugSpaceObjectCommand);

$queue->enqueue($rotateCommand);
//$queue->enqueue($debugSpaceObjectCommand);
*/


while (!$queue->isEmpty()) {
    try {
        $command = $queue->dequeue();
//        var_dump('command:', (new ReflectionClass($command))->getShortName());

        $command->execute();

        if ($command instanceof ExitGameCommand) {
            break;
        }
    } catch (Exception $exception) {
        (new ExceptionHandler($queue))->handler($command, $exception);
    }
}

var_dump('count:', $queue->count());
