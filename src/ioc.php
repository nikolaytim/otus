<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use App\Adapter\MoveAdapter;
use App\Adapter\RotateAdapter;
use App\Command\IocRegisterCommand;
use App\Ioc\Ioc;
use App\Ioc\Scope;
use App\Model\AbstractSpaceObject;
use App\Model\Direction;
use App\Model\Vector;

/**
 * Возвращает существующий скоуп
 */
Ioc::resolve('IocRegister', 'getScope', function (string $name) {
    $scopes = Ioc::getScopes();
    if (array_key_exists($name, $scopes)) {
        return $scopes[$name];
    }

    return null;
});

/**
 * Возвращает текущий скоуп
 */
Ioc::resolve('IocRegister', 'getCurrentScope', function () {
    return Ioc::getCurrentScope();
});

/**
 * Устанавливает текущий скоуп
 */
Ioc::resolve('IocRegister', 'setCurrentScope', function (Scope $scope) {
    Ioc::setCurrentScope($scope);
});

/**
 * Создает новый скоуп и устаналивает его текущим скоупом
 */
Ioc::resolve('IocRegister', 'createAndSetCurrentScope', function (string $name) {
    $rootScope = Ioc::resolve('getScope', 'Root');
    $scope = isset($rootScope) ? new Scope($name, $rootScope) : new Scope($name);

    Ioc::addScopeToScopes($name, $scope);
    Ioc::setCurrentScope($scope);
});

Ioc::resolve('createAndSetCurrentScope', 'SomeGame');


//Ioc::resolve('createAndSetCurrentScope', 'OtherGame');

/**
 * Константы (настройки)
 */
Ioc::resolve('IocRegister', 'getMaxPositionX', function () {
    return 800;
});
Ioc::resolve('IocRegister', 'getMaxPositionY', function () {
    return 600;
});
Ioc::resolve('IocRegister', 'getCountDirections', function () {
    return 8;
});
Ioc::resolve('IocRegister', 'getVelocity', function () {
    return rand(3, 8);
});

/**
 * Объекты Position и Direction
 */
Ioc::resolve('IocRegister', 'getPosition', function () {
    $x = rand(0, Ioc::resolve('getMaxPositionX'));
    $y = rand(0, Ioc::resolve('getMaxPositionY'));

    return new Vector($x, $y);
});

Ioc::resolve('IocRegister', 'getDirection', function () {
    $direction = rand(0, Ioc::resolve('getCountDirections'));

    return new Direction($direction, Ioc::resolve('getCountDirections'));
});

/**
 * Космические объекты
 */
Ioc::resolve('IocRegister', 'getSpaceObject', function () {
    $position = Ioc::resolve('getPosition');
    $direction = Ioc::resolve('getDirection');

    return new AbstractSpaceObject($position, $direction);
});

/**
 * Адаптеры
 */
Ioc::resolve('IocRegister', 'getMoveAdapter', function (AbstractSpaceObject $spaceObject) {
    return new MoveAdapter($spaceObject);
});

Ioc::resolve('IocRegister', 'getRotateAdapter', function (AbstractSpaceObject $spaceObject) {
    return new RotateAdapter($spaceObject);
});


$spaceObject = Ioc::resolve('getSpaceObject');
$spaceObject2 = Ioc::resolve('getSpaceObject');

//$moveAdapter = Ioc::resolve('getMoveAdapter', $spaceObject);
//dump('moveAdapter:', $moveAdapter);

//$moveAdapter2 = Ioc::resolve('getMoveAdapter', $spaceObject2);
//dump('moveAdapter2:', $moveAdapter2);

$rotateAdapter = Ioc::resolve('getRotateAdapter', $spaceObject);
//dump('rotateAdapter:', $rotateAdapter);

$rotateAdapter2 = Ioc::resolve('getRotateAdapter', $spaceObject2);
//dump('rotateAdapter2:', $rotateAdapter2);
/*
dump('currentScope:', Ioc::$currentScope);
dump('scopes:', Ioc::$scopes);
*/

//dump(Ioc::getScopes());

$scope = Ioc::resolve('getScope', 'Root');
dump('Root', $scope);

$scope2 = Ioc::resolve('getScope', 'SomeGame');
dump('SomeGame', $scope2);