<?php

namespace App\Adapter;

use App\Exception\GetPropertySpaceObjectException;
use App\Model\SpaceObjectInterface;

class CheckFuelAdapter implements CheckFuelInterface
{
    private SpaceObjectInterface $object;

    public function __construct(SpaceObjectInterface $object)
    {
        $this->object = $object;
    }

    public function getQuantityFuel(): int
    {
        return $this->object->getProperty('quantityFuel');
    }

    public function getRateFuel(): int
    {
        return $this->object->getProperty('rateFuel');
    }
}