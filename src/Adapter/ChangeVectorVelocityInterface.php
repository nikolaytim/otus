<?php

namespace App\Adapter;

use App\Model\Vector;

interface ChangeVectorVelocityInterface
{
    public function getVectorVelocity(): Vector;
    public function setVectorVelocity(Vector $velocity): void;
}