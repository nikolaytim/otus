<?php

namespace App\Adapter;

interface CheckFuelInterface
{
    public function getQuantityFuel(): int;
    public function getRateFuel(): int;
}