<?php

namespace App\Adapter;

use App\Exception\CommandException;
use App\Exception\GetPropertySpaceObjectException;
use App\Model\Vector;
use App\Model\SpaceObjectInterface;

class ChangeVectorVelocityAdapter implements ChangeVectorVelocityInterface
{
    private SpaceObjectInterface $object;

    public function __construct(SpaceObjectInterface $object)
    {
        $this->object = $object;
    }

    public function getVectorVelocity(): Vector
    {
        /**
         * Если скорость (velocity) не задана, то перехватываем исключение GetPropertySpaceObjectException
         * и выбрасываем исключение CommandException
         */
        try {
            $velocity = $this->object->getProperty('velocity');
        } catch (GetPropertySpaceObjectException $exception) {
            throw new CommandException('Скорость не определена!');
        }

        $direction = $this->object->getProperty('direction');

        return new Vector(
            (int) $velocity * cos($direction->getAngleByRadians()),
            (int) $velocity * sin($direction->getAngleByRadians()),
        );
    }

    public function setVectorVelocity(Vector $velocity): void
    {
        $this->object->setProperty('vectorVelocity', $velocity);
    }
}