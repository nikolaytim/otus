<?php

namespace App\Adapter;

use App\Model\SpaceObjectInterface;

class DebugAdapter implements DebugInterface
{
    private SpaceObjectInterface $object;

    public function __construct(SpaceObjectInterface $object)
    {
        $this->object = $object;
    }

    public function getSpaceObject(): SpaceObjectInterface
    {
        return $this->object;
    }
}