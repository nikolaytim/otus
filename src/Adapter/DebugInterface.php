<?php

namespace App\Adapter;

use App\Model\SpaceObjectInterface;

interface DebugInterface
{
    public function getSpaceObject(): SpaceObjectInterface;
}
