<?php

namespace App\Adapter;

use App\Exception\GetPropertySpaceObjectException;
use App\Model\Direction;
use App\Model\SpaceObjectInterface;
use App\Model\Vector;

class MoveAdapter implements MoveInterface
{
    private SpaceObjectInterface $object;

    public function __construct(SpaceObjectInterface $object)
    {
        $this->object = $object;
    }

    public function setPosition(Vector $position): void
    {
        $this->object->setProperty('position', $position);
    }

    public function getPosition(): Vector
    {
        $position = $this->object->getProperty('position');
        if (!$position instanceof Vector) {
            throw new GetPropertySpaceObjectException('Свойство: position не типа Vector!');
        }

        return $position;
    }

    public function getVectorVelocity(): Vector
    {
        $direction = $this->object->getProperty('direction');
        $velocity = $this->object->getProperty('velocity');

        return new Vector(
            (int) $velocity * cos($direction->getAngleByRadians()),
            (int) $velocity * sin($direction->getAngleByRadians()),
        );
    }
}