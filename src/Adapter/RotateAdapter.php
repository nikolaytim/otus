<?php

namespace App\Adapter;

use App\Exception\GetPropertySpaceObjectException;
use App\Model\SpaceObjectInterface;
use App\Model\Direction;

class RotateAdapter implements RotateInterface
{
    private SpaceObjectInterface $object;

    public function __construct(SpaceObjectInterface $object)
    {
        $this->object = $object;
    }

    public function getDirection(): Direction
    {
        $direction = $this->object->getProperty('direction');
        if (!$direction instanceof Direction) {
            throw new GetPropertySpaceObjectException('Свойство: direction не типа Direction!');
        }

        return $direction;
    }

    public function setDirection(Direction $direction): void
    {
        $this->object->setProperty('direction', $direction);
    }

    public function getAngularVelocity(): int
    {
        return 1;
    }
}