<?php

namespace App\Adapter;

interface FuelInterface
{
    public function setQuantityFuel(int $quantityFuel): void;
    public function getQuantityFuel(): int;
    public function getRateFuel(): int;
}