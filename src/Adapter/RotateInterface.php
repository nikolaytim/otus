<?php

namespace App\Adapter;

use App\Model\Direction;

interface RotateInterface
{
    public function getDirection(): Direction;
    public function setDirection(Direction $direction): void;
    public function getAngularVelocity(): int;
}