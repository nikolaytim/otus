<?php

namespace App\Adapter;

use App\Exception\GetPropertySpaceObjectException;
use App\Model\SpaceObjectInterface;

class FuelAdapter implements FuelInterface
{
    private SpaceObjectInterface $object;

    public function __construct(SpaceObjectInterface $object)
    {
        $this->object = $object;
    }

    public function setQuantityFuel(int $quantityFuel): void
    {
        $this->object->setProperty('quantityFuel', $quantityFuel);
    }

    public function getQuantityFuel(): int
    {
        return $this->object->getProperty('quantityFuel');
    }

    public function getRateFuel(): int
    {
        return $this->object->getProperty('rateFuel');
    }
}