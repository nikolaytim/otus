<?php

namespace App\Adapter;

use App\Model\Vector;

interface MoveInterface
{
    public function setPosition(Vector $position): void;
    public function getPosition(): Vector;
    public function getVectorVelocity(): Vector;
}