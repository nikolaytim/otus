<?php

namespace App\Command;

use App\Exception\CommandException;

class MoveWithFuelCommand implements CommandInterface
{
    private array $commands;

    public function __construct(array $commands)
    {
        $this->commands = $commands;
    }

    public function execute(): void
    {
        try {
            foreach ($this->commands as $command) {
                $command->execute();
            }
        } catch (CommandException $exception) {
            var_dump('MoveWithFuelCommand, CommandException:', $exception->getMessage());
        }
    }
}