<?php

namespace App\Command;

use App\Adapter\FuelInterface;

class FuelCommand implements CommandInterface
{
    private FuelInterface $fuelAdapter;

    public function __construct(FuelInterface $fuelAdapter)
    {
        $this->fuelAdapter = $fuelAdapter;
    }

    public function execute(): void
    {
        $quantityFuel = $this->fuelAdapter->getQuantityFuel();
        $rateFuel = $this->fuelAdapter->getRateFuel();
        $this->fuelAdapter->setQuantityFuel($quantityFuel - $rateFuel);
    }
}