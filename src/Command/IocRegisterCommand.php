<?php

namespace App\Command;

use App\Ioc\Ioc;

class IocRegisterCommand implements CommandInterface
{
    private string $key;
    private \Closure $callback;

    public function __construct(string $key, \Closure $callback)
    {
        $this->key = $key;
        $this->callback = $callback;
    }

    public function execute(): void
    {
        $currentScope = Ioc::resolve('getCurrentScope');
        $currentScope->set($this->key, $this->callback);
    }
}