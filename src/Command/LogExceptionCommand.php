<?php

namespace App\Command;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class LogExceptionCommand implements CommandInterface
{
    private CommandInterface $command;
    private \Exception $exception;

    public function __construct(CommandInterface $command, \Exception $exception)
    {
        $this->command = $command;
        $this->exception = $exception;
    }

    public function getLogger(): LoggerInterface
    {
        $logger = new Logger('default');
        $logger->pushHandler(new StreamHandler('./logger.log', Logger::DEBUG));

        return $logger;
    }

    public function execute(): void
    {
        $logger = $this->getLogger();
        $logger->debug('Command: ' . get_class($this->command));
        $logger->debug('Exception: ' . get_class($this->exception));
    }
}