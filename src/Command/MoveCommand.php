<?php

namespace App\Command;

use App\Adapter\MoveInterface;

class MoveCommand implements CommandInterface
{
    private MoveInterface $moveAdapter;

    public function __construct(MoveInterface $moveAdapter)
    {
        $this->moveAdapter = $moveAdapter;
    }

    public function execute(): void
    {
        $velocity = $this->moveAdapter->getVectorVelocity();
        $position = $this->moveAdapter->getPosition();
        $newPosition = $position->plus($velocity);
        $this->moveAdapter->setPosition($newPosition);
    }
}