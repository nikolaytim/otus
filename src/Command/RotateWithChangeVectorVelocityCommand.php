<?php

namespace App\Command;

use App\Exception\CommandException;

class RotateWithChangeVectorVelocityCommand implements CommandInterface
{
    private array $commands;

    public function __construct(array $commands)
    {
        $this->commands = $commands;
    }

    public function execute(): void
    {
        foreach ($this->commands as $command) {
            try {
                $command->execute();
            } catch (CommandException $exception) {
                var_dump('RotateWithChangeVectorVelocityCommand, CommandException:', $exception->getMessage());
            }
        }
    }
}