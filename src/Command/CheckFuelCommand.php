<?php

namespace App\Command;

use App\Adapter\CheckFuelInterface;
use App\Exception\CommandException;

class CheckFuelCommand implements CommandInterface
{
    private CheckFuelInterface $checkFuelAdapter;

    public function __construct(CheckFuelInterface $checkFuelAdapter)
    {
        $this->checkFuelAdapter = $checkFuelAdapter;
    }

    public function execute(): void
    {
        $quantityFuel = $this->checkFuelAdapter->getQuantityFuel();
        $rateFuel = $this->checkFuelAdapter->getRateFuel();

        if ($rateFuel > $quantityFuel) {
            throw new CommandException('Недостаточно топлива!');
        }
    }
}