<?php

namespace App\Command;

use App\Adapter\ChangeVectorVelocityInterface;

class ChangeVectorVelocityCommand implements CommandInterface
{
    private ChangeVectorVelocityInterface $changeVectorVelocityAdapter;

    public function __construct(ChangeVectorVelocityInterface $changeVectorVelocityAdapter)
    {
        $this->changeVectorVelocityAdapter = $changeVectorVelocityAdapter;
    }

    public function execute(): void
    {
        $velocity = $this->changeVectorVelocityAdapter->getVectorVelocity();
        $this->changeVectorVelocityAdapter->setVectorVelocity($velocity);
    }
}