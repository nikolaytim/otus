<?php

namespace App\Command;

use App\Adapter\DebugInterface;

class DebugSpaceObjectCommand implements CommandInterface
{
    private DebugInterface $adapter;

    public function __construct(DebugInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    public function execute(): void
    {
        $spaceObject = $this->adapter->getSpaceObject();
        var_dump('Debug SpaceObject:', $spaceObject);
    }
}
