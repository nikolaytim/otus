<?php

namespace App\Command;

use App\Ioc\Ioc;
use App\Ioc\Scope;

class InitIocRootScopeCommand implements CommandInterface
{
    public function execute(): void
    {
        $scope = new Scope('Root');
        $scope->set('IocRegister', function ($key, $callback) {
            return new IocRegisterCommand($key, $callback);
        });
        $scope->set('getCurrentScope', function () {
            return Ioc::getCurrentScope();
        });
        $scope->set('setCurrentScope', function (Scope $scope) {
            return Ioc::setCurrentScope($scope);
        });
        $scope->set('getScopes', function () {
            return Ioc::getScopes();
        });
        $scope->set('getScope', function (string $name) {
            return Ioc::getScope($name);
        });
        $scope->set('addScopeToScopes', function (string $name, Scope $scope) {
            return Ioc::addScopeToScopes($name, $scope);
        });

        Ioc::setCurrentScope($scope);
        Ioc::addScopeToScopes('Root', $scope);
    }
}