<?php

namespace App\Command;

use App\Adapter\RotateInterface;

class RotateCommand implements CommandInterface
{
    private RotateInterface $rotateAdapter;

    public function __construct(RotateInterface $rotateAdapter)
    {
        $this->rotateAdapter = $rotateAdapter;
    }

    public function execute(): void
    {
        $direction = $this->rotateAdapter->getDirection();
        $angularVelocity = $this->rotateAdapter->getAngularVelocity();
        $newDirection = $direction->next($angularVelocity);
        $this->rotateAdapter->setDirection($newDirection);
    }
}