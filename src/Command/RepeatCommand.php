<?php

namespace App\Command;

class RepeatCommand implements CommandInterface
{
    private CommandInterface $command;

    public function __construct(CommandInterface $command)
    {
        $this->command = $command;
    }

    public function execute(): void
    {
        $this->command->execute();
    }
}