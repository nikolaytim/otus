<?php

namespace App\Ioc;

class Scope
{
    private string $name;
    private array $container = [];
    private ?Scope $parent;

    public function __construct(string $name, ?Scope $parent = null)
    {
        $this->name = $name;
        $this->parent = $parent;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getContainer(): array
    {
        return $this->container;
    }

    public function getParent(): ?Scope
    {
        return $this->parent;
    }

    public function setParent(Scope $parent): void
    {
        $this->parent = $parent;
    }

    public function get(string $key): ?\Closure
    {
        if (array_key_exists($key, $this->container)) {
            return $this->container[$key];
        }

        $parent = $this->getParent();
        if ($parent) {
            return $parent->get($key);
        }

        return null;
    }

    public function set(string $key, \Closure $callback): void
    {
        $this->container[$key] = $callback;
    }
}