<?php

namespace App\Ioc;

use App\Exception\IocException;

class Ioc implements IocInterface
{
    private static array $scopes = [];
    private static ?Scope $currentScope;

    public static function resolve(string $key, ...$args)
    {
        if (!isset(self::$currentScope)) {
            throw new IocException(
                'Текущий скоуп не установлен!'
            );
        }

        $callback = self::$currentScope->get($key);

        /**
         * callback уже зарегистрирован - разрешаем зависимость
         */
        if ($callback && is_callable($callback)) {
            try {
                return $callback(...$args);
            } catch (\TypeError|\Exception $exception) {
                throw new IocException(
                    'Ошибка при разрешении зависимости - ' . $key . ': ' . $exception->getMessage()
                );
            }
        }

        throw new IocException(
            'Зависимость - ' . $key . ': не зарегистрирована в текущем скоупе!'
        );
    }

    /**
     * Скоупы ($scopes) и текущий скоуп ($currentScope) сделал приватными.
     * Поэтому скоупы разрешаются и через resolve (после выполнения InitIocRootScopeCommand),
     * и через следующие методы:
     * - Ioc::getCurrentScope()
     * - Ioc::setCurrentScope(Scope $scope)
     * - Ioc::getScopes()
     * - Ioc::getScope(string $name)
     * - Ioc::addScopeToScopes(string $name, Scope $scope)
     *
     * Если скоупы и текущий скоуп сделать публичными, то данные методы не понадобятся
     */
    public static function __callStatic($method, $args)
    {
        switch ($method) {
            case 'getCurrentScope':
                return self::$currentScope ?? null;
                break;

            case 'setCurrentScope':
                self::$currentScope = $args[0];
                break;

            case 'getScopes':
                return self::$scopes;
                break;

            case 'getScope':
                return self::$scopes[$args[0]];
                break;

            case 'addScopeToScopes':
                self::$scopes[$args[0]] = $args[1];
        }
    }
}