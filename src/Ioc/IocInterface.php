<?php

namespace App\Ioc;

interface IocInterface
{
    public static function resolve(string $key, ...$args);
}