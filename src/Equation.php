<?php

namespace App;

class Equation
{
    private static $instance = null;

    private function __construct() {}

    public static function getInstance()
    {
        if (!isset(self::$instance))
            self::$instance = new self();

        return self::$instance;
    }

    public function verifyFloat(string $nameParam, float $valueParam): void
    {
        if (is_nan($valueParam) || is_infinite($valueParam)) 
            throw new \Exception('Некорректное значение параметра: ' . $nameParam . '!');
    }

    public function verifyParams(float $a, float $b, float $c, float $epsilon): void
    {
        $this->verifyFloat('a', $a);
        $this->verifyFloat('b', $b);
        $this->verifyFloat('c', $c);
        $this->verifyFloat('epsilon', $epsilon);

        if (abs($a) < $epsilon) {
            throw new \Exception('Коэффициент a не может быть равен 0!');
        }
    }

    public function solve(float $a, float $b, float $c, float $epsilon = 1E-10): array
    {
        $this->verifyParams($a, $b, $c, $epsilon);

        $d = $b * $b - 4.0 * $a * $c;
        if ($d < -$epsilon) {
            return [];
        } elseif (abs($d) <= $epsilon) {
            return [-1.0 * ($b / (2.0 * $a))];
        } else {
            $firstValue = (-1.0 * $b + floatval(sqrt($d))) / (2.0 * $a);
            $secondValue = (-1.0 * $b - floatval(sqrt($d))) / (2.0 * $a);
            return [$firstValue, $secondValue];
        }
    }
}