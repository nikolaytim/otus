<?php

namespace App\Model;

use App\Exception\GetPropertySpaceObjectException;
use App\Exception\SetPropertySpaceObjectException;

class AbstractSpaceObject implements SpaceObjectInterface
{
    private Vector $position;
    private Direction $direction;

    public function __construct(Vector $position, Direction $direction)
    {
        $this->position = $position;
        $this->direction = $direction;
    }

    public function getProperty(string $key)
    {
        if (!isset($this->$key)) {
            throw new GetPropertySpaceObjectException('Свойство: ' . $key . ' не найдено!');
        }

        return $this->$key;
    }

    public function setProperty(string $key, $value): void
    {
        if (!isset($this->$key)) {
            $this->$key = $value;
            return;
        }

        if (gettype($value) !== gettype($this->$key)) {
            throw new SetPropertySpaceObjectException('Ошибка установки нового значения свойства: ' . $key . '!');
        }

        if (!is_object($this->$key) && !is_object($value)) {
            $this->$key = $value;
            return;
        }

        $classProperty = get_class($this->$key);
        if ($value instanceof $classProperty) {
            $this->$key = $value;
        } else {
            throw new SetPropertySpaceObjectException('Ошибка установки нового значения свойства: ' . $key . '!');
        }
    }
}