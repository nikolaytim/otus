<?php

namespace App\Model;

interface SpaceObjectInterface
{
    public function getProperty(string $key);
    public function setProperty(string $key, $value);
}