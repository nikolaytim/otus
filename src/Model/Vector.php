<?php

namespace App\Model;

class Vector
{
    private int $x;
    private int $y;

    public function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function setX(int $x): void
    {
        $this->x = $x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function setY(int $y): void
    {
        $this->y = $y;
    }

    public function plus(self $value): self
    {
        return new self(
            $this->getX() + $value->getX(),
            $this->getY() + $value->getY()
        );
    }
}