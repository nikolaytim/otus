<?php

namespace App\Model;

class Direction
{
    private int $direction;
    private int $countDirections;

    public function __construct(int $direction, int $countDirections)
    {
        $this->direction = $direction;
        $this->countDirections = $countDirections;
    }

    public function getDirection(): int
    {
        return $this->direction;
    }

    public function setDirection(int $direction): void
    {
        $this->direction = $direction;
    }

    public function getCountDirections(): int
    {
        return $this->countDirections;
    }

    public function setCountDirections(int $countDirections): void
    {
        $this->countDirections = $countDirections;
    }

    public function next(int $angularVelocity): self
    {
        return new self(
            ($this->getDirection() + $angularVelocity) % $this->getCountDirections(),
            $this->getCountDirections()
        );
    }

    public function getAngleByRadians(): float
    {
        return (float) $this->getDirection() * 2. * M_PI / (float) $this->getCountDirections();
    }
}