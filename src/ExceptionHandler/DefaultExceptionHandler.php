<?php

namespace App\ExceptionHandler;

use SplQueue;

class DefaultExceptionHandler implements ExceptionHandlerInterface
{
    private SplQueue $queue;

    public function __construct(SplQueue $queue)
    {
        $this->queue = $queue;
    }

    public function handler($command, $exception): void
    {
        var_dump('DefaultExceptionHandler->handler! command:', get_class($command));
        var_dump('DefaultExceptionHandler->handler! exception:', get_class($exception));
        var_dump('DefaultExceptionHandler->handler! exception message:', $exception->getMessage());
    }
}