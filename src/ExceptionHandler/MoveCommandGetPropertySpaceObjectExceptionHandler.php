<?php

namespace App\ExceptionHandler;

use App\Command\CommandInterface;
use App\Command\RepeatCommand2;
use SplQueue;

class MoveCommandGetPropertySpaceObjectExceptionHandler implements ExceptionHandlerInterface
{
    private SplQueue $queue;

    public function __construct(SplQueue $queue)
    {
        $this->queue = $queue;
    }

    public function handler(CommandInterface $command, \Exception $exception): void
    {
        $this->queue->enqueue(new RepeatCommand2($command));
    }
}