<?php

namespace App\ExceptionHandler;

use App\Command\CommandInterface;
use App\Command\RepeatTwiceCommand2;
use SplQueue;

class RepeatCommand2GetPropertySpaceObjectExceptionHandler implements ExceptionHandlerInterface
{
    private SplQueue $queue;

    public function __construct(SplQueue $queue)
    {
        $this->queue = $queue;
    }

    public function handler(CommandInterface $command, \Exception $exception): void
    {
        $logExceptionGameCommand = new RepeatTwiceCommand2($command, $exception);
        $this->queue->enqueue($logExceptionGameCommand);
    }
}