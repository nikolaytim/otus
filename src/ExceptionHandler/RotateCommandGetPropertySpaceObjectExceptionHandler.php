<?php

namespace App\ExceptionHandler;

use App\Command\CommandInterface;
use App\Command\RepeatCommand;
use SplQueue;

class RotateCommandGetPropertySpaceObjectExceptionHandler implements ExceptionHandlerInterface
{
    private SplQueue $queue;

    public function __construct(SplQueue $queue)
    {
        $this->queue = $queue;
    }

    public function handler(CommandInterface $command, \Exception $exception): void
    {
        $this->queue->enqueue(new RepeatCommand($command));
    }
}