<?php

namespace App\ExceptionHandler;

use App\Command\CommandInterface;

interface ExceptionHandlerInterface
{
    public function handler(CommandInterface $command, \Exception $exception): void;
}