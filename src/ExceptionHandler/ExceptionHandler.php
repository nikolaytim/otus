<?php

namespace App\ExceptionHandler;

use App\Command\CommandInterface;
use SplQueue;

/**
 * Словаря исключений нет.
 *
 * Handler-ы обработки исключений имеют следующее имя класса: CommandClass + ExceptionClass + 'Handler',
 * например: для MoveCommand и исключения GetPropertySpaceObjectException получаем следующий handler:
 * MoveCommandGetPropertySpaceObjectExceptionHandler
 *
 * Если handler не найден используется дефолтный: DefaultExceptionHandler
 */
class ExceptionHandler implements ExceptionHandlerInterface
{
    const HANDLER = 'Handler';
    const NAMESPACE = 'App\ExceptionHandler\\';
    const DEFAULT_EXCEPTION_HANDLER = 'DefaultExceptionHandler';

    private SplQueue $queue;

    public function __construct(SplQueue $queue)
    {
        $this->queue = $queue;
    }

    public function resolve(CommandInterface $command, \Exception $exception): string
    {
        $classCommand = (new \ReflectionClass($command))->getShortName();
        $classException = (new \ReflectionClass($exception))->getShortName();
        $exceptionHandler = self::NAMESPACE . $classCommand . $classException . self::HANDLER;

        if (class_exists($exceptionHandler) && method_exists($exceptionHandler, 'handler')) {
            return $exceptionHandler;
        }

        return self::NAMESPACE . self::DEFAULT_EXCEPTION_HANDLER;
    }

    public function handler(CommandInterface $command, \Exception $exception): void
    {
        var_dump($exception->getMessage());

        $exceptionHandler = $this->resolve($command, $exception);
        (new $exceptionHandler($this->queue))->handler($command, $exception);
    }
}