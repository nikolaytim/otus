<?php

namespace App\ExceptionHandler;

use App\Command\CommandInterface;
use App\Command\LogExceptionCommand;
use SplQueue;

class LogExceptionHandler implements ExceptionHandlerInterface
{
    private SplQueue $queue;

    public function __construct(SplQueue $queue)
    {
        $this->queue = $queue;
    }

    public function handler(CommandInterface $command, \Exception $exception): void
    {
        $logExceptionCommand = new LogExceptionCommand($command, $exception);
        $this->queue->enqueue($logExceptionCommand);
    }
}