<?php

namespace UnitTests\Ioc;

use App\Adapter\MoveAdapter;
use App\Command\InitIocRootScopeCommand;
use App\Command\MoveCommand;
use App\Ioc\Ioc;
use App\Model\AbstractSpaceObject;
use App\Model\Direction;
use App\Model\Vector;
use PHPUnit\Framework\TestCase;

class IocTest extends TestCase
{
    public function resolveDataProvider(): array
    {
        return [
            'register_getMaxPositionX' => [
                800,
                [
                    'register' => 'IocRegister',
                    'key' => 'getMaxPositionX',
                    'callback' => function () {
                        return 800;
                    },
                ]
            ],
            'register_getMaxPositionY' => [
                600,
                [
                    'register' => 'IocRegister',
                    'key' => 'getMaxPositionY',
                    'callback' => function () {
                        return 600;
                    },
                ]
            ],
            'register_getCountDirections' => [
                8,
                [
                    'register' => 'IocRegister',
                    'key' => 'getCountDirections',
                    'callback' => function () {
                        return 8;
                    },
                ]
            ],
            'register_getPosition' => [
                'App\Model\Vector',
                [
                    'register' => 'IocRegister',
                    'key' => 'getPosition',
                    'callback' => function () {
                        $x = rand(0, Ioc::resolve('getMaxPositionX'));
                        $y = rand(0, Ioc::resolve('getMaxPositionY'));

                        return new Vector($x, $y);
                    },
                ]
            ],
            'register_getDirection' => [
                'App\Model\Direction',
                [
                    'register' => 'IocRegister',
                    'key' => 'getDirection',
                    'callback' => function () {
                        $direction = rand(0, Ioc::resolve('getCountDirections'));

                        return new Direction($direction, Ioc::resolve('getCountDirections'));
                    },
                ]
            ],
            'register_getSpaceObject' => [
                'App\Model\AbstractSpaceObject',
                [
                    'register' => 'IocRegister',
                    'key' => 'getSpaceObject',
                    'callback' => function () {
                        $position = Ioc::resolve('getPosition');
                        $direction = Ioc::resolve('getDirection');

                        return new AbstractSpaceObject($position, $direction);
                    },
                ]
            ],
            'register_getMoveAdapter' => [
                'App\Adapter\MoveAdapter',
                [
                    'register' => 'IocRegister',
                    'key' => 'getMoveAdapter',
                    'callback' => function (AbstractSpaceObject $spaceObject) {
                        return new MoveAdapter($spaceObject);
                    },
                ]
            ],
            'register_getMoveCommand' => [
                'App\Command\MoveCommand',
                [
                    'register' => 'IocRegister',
                    'key' => 'getMoveCommand',
                    'callback' => function (MoveAdapter $moveAdapter) {
                        return new MoveCommand($moveAdapter);
                    },
                ]
            ],
        ];
    }

    /**
     * @dataProvider resolveDataProvider
     */
    public function testResolveMethod($result, array $params): void
    {
        if (!Ioc::getCurrentScope()) {
            (new InitIocRootScopeCommand())->execute();
        }

        Ioc::resolve($params['register'], $params['key'], $params['callback'])->execute();
        $this->assertEquals(true, array_key_exists($params['key'], Ioc::resolve('getCurrentScope')->getContainer()));

        if (is_int($result)) {
            $this->assertEquals($result, Ioc::resolve($params['key']));
        } else {

            if ($result === 'App\Adapter\MoveAdapter') {
                $object = Ioc::resolve($params['key'], Ioc::resolve('getSpaceObject'));
            } elseif ($result === 'App\Command\MoveCommand') {
                $object = Ioc::resolve(
                    $params['key'],
                    Ioc::resolve('getMoveAdapter', Ioc::resolve('getSpaceObject'))
                );
            }
            else {
                $object = Ioc::resolve($params['key']);
            }

            $this->assertEquals(true, $object instanceof $result);
        }
    }
}