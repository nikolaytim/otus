<?php

namespace UnitTests\ExceptionHandler;

use App\Command\MoveCommand;
use App\Exception\GetPropertySpaceObjectException;
use App\ExceptionHandler\MoveCommandGetPropertySpaceObjectExceptionHandler;
use PHPUnit\Framework\TestCase;
use SplQueue;

class MoveCommandGetPropertySpaceObjectExceptionHandlerTest extends TestCase
{
    public function testHandler(): void
    {
        $queue = $this->createMock(SplQueue::class);
        $command = $this->createMock(MoveCommand::class);
        $exception = $this->createMock(GetPropertySpaceObjectException::class);

        $queue
            ->expects($this->once())
            ->method('enqueue');

        $moveCommandGetPropertySpaceObjectExceptionHandler =
            new MoveCommandGetPropertySpaceObjectExceptionHandler($queue);
        $moveCommandGetPropertySpaceObjectExceptionHandler->handler($command, $exception);
    }
}