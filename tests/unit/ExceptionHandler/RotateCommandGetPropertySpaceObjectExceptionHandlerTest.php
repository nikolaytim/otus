<?php

namespace UnitTests\ExceptionHandler;

use App\Command\CommandInterface;
use App\ExceptionHandler\RotateCommandGetPropertySpaceObjectExceptionHandler;
use PHPUnit\Framework\TestCase;
use SplQueue;

class RotateCommandGetPropertySpaceObjectExceptionHandlerTest extends TestCase
{
    public function testHandler(): void
    {
        $queue = $this->createMock(SplQueue::class);
        $command = $this->createMock(CommandInterface::class);
        $exception = $this->createMock(\Exception::class);

        $queue
            ->expects($this->once())
            ->method('enqueue');

        $rotateCommandGetPropertySpaceObjectExceptionHandler =
            new RotateCommandGetPropertySpaceObjectExceptionHandler($queue);
        $rotateCommandGetPropertySpaceObjectExceptionHandler->handler($command, $exception);
    }
}