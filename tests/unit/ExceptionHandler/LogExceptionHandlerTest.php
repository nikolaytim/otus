<?php

namespace UnitTests\ExceptionHandler;

use App\Command\CommandInterface;
use App\Command\LogExceptionCommand;
use App\ExceptionHandler\LogExceptionHandler;
use PHPUnit\Framework\TestCase;
use SplQueue;

class LogExceptionHandlerTest extends TestCase
{
    public function testHandler(): void
    {
        $queue = $this->createMock(SplQueue::class);
        $command = $this->createMock(CommandInterface::class);
        $exception = $this->createMock(\Exception::class);

        $queue
            ->expects($this->once())
            ->method('enqueue');

        $logExceptionHandler = new LogExceptionHandler($queue);
        $logExceptionHandler->handler($command, $exception);
    }
}