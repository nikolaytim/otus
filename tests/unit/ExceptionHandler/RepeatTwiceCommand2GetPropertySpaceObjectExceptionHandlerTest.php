<?php

namespace UnitTests\ExceptionHandler;

use App\Command\CommandInterface;
use App\ExceptionHandler\RepeatTwiceCommand2GetPropertySpaceObjectExceptionHandler;
use PHPUnit\Framework\TestCase;
use SplQueue;

class RepeatTwiceCommand2GetPropertySpaceObjectExceptionHandlerTest extends TestCase
{
    public function testHandler(): void
    {
        $queue = $this->createMock(SplQueue::class);
        $command = $this->createMock(CommandInterface::class);
        $exception = $this->createMock(\Exception::class);

        $queue
            ->expects($this->once())
            ->method('enqueue');

        $repeatTwiceCommand2GetPropertySpaceObjectExceptionHandler =
            new RepeatTwiceCommand2GetPropertySpaceObjectExceptionHandler($queue);
        $repeatTwiceCommand2GetPropertySpaceObjectExceptionHandler->handler($command, $exception);
    }
}