<?php

namespace UnitTests\ExceptionHandler;

use App\Command\RepeatCommand2;
use App\Exception\GetPropertySpaceObjectException;
use App\ExceptionHandler\RepeatCommand2GetPropertySpaceObjectExceptionHandler;
use PHPUnit\Framework\TestCase;
use SplQueue;

class RepeatCommand2GetPropertySpaceObjectExceptionHandlerTest extends TestCase
{
    public function testHandler(): void
    {
        $queue = $this->createMock(SplQueue::class);
        $command = $this->createMock(RepeatCommand2::class);
        $exception = $this->createMock(GetPropertySpaceObjectException::class);

        $queue
            ->expects($this->once())
            ->method('enqueue');

        $repeatCommand2GetPropertySpaceObjectExceptionHandler =
            new RepeatCommand2GetPropertySpaceObjectExceptionHandler($queue);
        $repeatCommand2GetPropertySpaceObjectExceptionHandler->handler($command, $exception);
    }
}