<?php

namespace UnitTests\ExceptionHandler;

use App\Command\CommandInterface;
use App\ExceptionHandler\RepeatCommandGetPropertySpaceObjectExceptionHandler;
use PHPUnit\Framework\TestCase;
use SplQueue;

class RepeatCommandGetPropertySpaceObjectExceptionHandlerTest extends TestCase
{
    public function testHandler(): void
    {
        $queue = $this->createMock(SplQueue::class);
        $command = $this->createMock(CommandInterface::class);
        $exception = $this->createMock(\Exception::class);

        $queue
            ->expects($this->once())
            ->method('enqueue');

        $repeatCommandGetPropertySpaceObjectExceptionHandler =
            new RepeatCommandGetPropertySpaceObjectExceptionHandler($queue);
        $repeatCommandGetPropertySpaceObjectExceptionHandler->handler($command, $exception);
    }
}