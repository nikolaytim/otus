<?php

namespace UnitTests\Command;

use App\Adapter\RotateInterface;
use App\Command\RotateCommand;
use App\Exception\GetPropertySpaceObjectException;
use App\Exception\SetPropertySpaceObjectException;
use App\Model\Direction;
use Mockery;
use PHPUnit\Framework\TestCase;

class RotateCommandTest extends TestCase
{
    use \Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function tearDown(): void
    {
        Mockery::close();
    }

    public function executeDataProvider(): array
    {
        return [
            'rotate_1' => [
                2,
                [
                    'direction' => 1,
                    'countDirections' => 8,
                    'angularVelocity' => 1,
                ],
            ],
            'rotate_2' => [
                6,
                [
                    'direction' => 2,
                    'countDirections' => 16,
                    'angularVelocity' => 4,
                ],
            ],
            'rotate_3' => [
                2,
                [
                    'direction' => 18,
                    'countDirections' => 20,
                    'angularVelocity' => 4,
                ],
            ],
        ];
    }

    /**
     * @dataProvider executeDataProvider
     */
    public function testExecute(int $result, array $params): void
    {
        $direction = new Direction($params['direction'], $params['countDirections']);
        $directionResult = new Direction($result, $params['countDirections']);

        $rotateInterface = $this->createMock(RotateInterface::class);
        $rotateInterface
            ->method('getDirection')
            ->willReturn($direction);

        $rotateInterface
            ->method('getAngularVelocity')
            ->willReturn($params['angularVelocity']);

        $rotateInterface
            ->expects($this->once())
            ->method('setDirection')
            ->with($directionResult);

        $rotateCommand = new RotateCommand($rotateInterface);
        $rotateCommand->execute();
    }

    public function testGetDirectionException(): void
    {
        $rotateInterface = $this->createMock(RotateInterface::class);
        $rotateInterface
            ->method('getDirection')
            ->willThrowException(new GetPropertySpaceObjectException('Свойство: direction не типа Direction!'));

        $this->expectException(GetPropertySpaceObjectException::class);
        $this->expectExceptionMessage('Свойство: direction не типа Direction!');

        $rotateCommand = new RotateCommand($rotateInterface);
        $rotateCommand->execute();
    }

    public function testSetDirectionException(): void
    {
        $rotateInterface = $this->createMock(RotateInterface::class);
        $rotateInterface
            ->method('setDirection')
            ->willThrowException(
                new SetPropertySpaceObjectException('Ошибка установки нового значения свойства: direction!')
            );

        $this->expectException(SetPropertySpaceObjectException::class);
        $this->expectExceptionMessage('Ошибка установки нового значения свойства: direction!');

        $rotateCommand = new RotateCommand($rotateInterface);
        $rotateCommand->execute();
    }
}