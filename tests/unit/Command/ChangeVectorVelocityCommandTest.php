<?php

namespace UnitTests\Command;

use App\Adapter\ChangeVectorVelocityInterface;
use App\Command\ChangeVectorVelocityCommand;
use App\Exception\GetPropertySpaceObjectException;
use App\Exception\SetPropertySpaceObjectException;
use App\Model\Vector;
use PHPUnit\Framework\TestCase;

class ChangeVectorVelocityCommandTest extends TestCase
{
    public function testExecute(): void
    {
        $velocityX = 3;
        $velocityY = -3;
        $velocity = new Vector($velocityX, $velocityY);

        $changeVectorVelocityInterface = $this->createMock(ChangeVectorVelocityInterface::class);

        $changeVectorVelocityInterface
            ->method('getVectorVelocity')
            ->willReturn($velocity);

        $changeVectorVelocityInterface
            ->expects($this->once())
            ->method('setVectorVelocity')
            ->with($velocity);

        $changeVectorVelocityCommand = new ChangeVectorVelocityCommand($changeVectorVelocityInterface);
        $changeVectorVelocityCommand->execute();
    }

    public function testGetVectorVelocityException(): void
    {
        $changeVectorVelocityInterface = $this->createMock(ChangeVectorVelocityInterface::class);
        $changeVectorVelocityInterface
            ->method('getVectorVelocity')
            ->willThrowException(new GetPropertySpaceObjectException('Свойство: velocity не найдено!'));

        $this->expectException(GetPropertySpaceObjectException::class);
        $this->expectExceptionMessage('Свойство: velocity не найдено!');

        $changeVectorVelocityCommand = new ChangeVectorVelocityCommand($changeVectorVelocityInterface);
        $changeVectorVelocityCommand->execute();
    }

    public function testSetVectorVelocityException(): void
    {
        $changeVectorVelocityInterface = $this->createMock(ChangeVectorVelocityInterface::class);
        $changeVectorVelocityInterface
            ->method('setVectorVelocity')
            ->willThrowException(
                new SetPropertySpaceObjectException('Ошибка установки нового значения свойства: vectorVelocity!')
            );

        $this->expectException(SetPropertySpaceObjectException::class);
        $this->expectExceptionMessage('Ошибка установки нового значения свойства: vectorVelocity!');

        $changeVectorVelocityCommand = new ChangeVectorVelocityCommand($changeVectorVelocityInterface);
        $changeVectorVelocityCommand->execute();
    }
}