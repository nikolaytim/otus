<?php

namespace UnitTests\Command;

use App\Command\CommandInterface;
use App\Command\RepeatCommand2;
use PHPUnit\Framework\TestCase;

class RepeatCommand2Test extends TestCase
{
    public function testExecute(): void
    {
        $command = $this->createMock(CommandInterface::class);
        $command
            ->expects($this->once())
            ->method('execute');

        $repeatCommand2 = new RepeatCommand2($command);
        $repeatCommand2->execute();
    }
}
