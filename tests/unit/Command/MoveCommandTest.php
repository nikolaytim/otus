<?php

namespace UnitTests\Command;

use App\Adapter\MoveInterface;
use App\Command\MoveCommand;
use App\Exception\GetPropertySpaceObjectException;
use App\Exception\SetPropertySpaceObjectException;
use App\Model\Vector;
use PHPUnit\Framework\TestCase;

class MoveCommandTest extends TestCase
{
    public function executeDataProvider(): array
    {
        return [
            'move_1' => [
                [
                    'newPositionX' => 5,
                    'newPositionY' => 8
                ],
                [
                    'positionX' => 12,
                    'positionY' => 5,
                    'velocityX' => -7,
                    'velocityY' => 3,
                ],
            ],
            'move_2' => [
                [
                    'newPositionX' => 20,
                    'newPositionY' => 9
                ],
                [
                    'positionX' => 15,
                    'positionY' => 12,
                    'velocityX' => 5,
                    'velocityY' => -3,
                ],
            ],
        ];
    }

    /**
     * @dataProvider executeDataProvider
     */
    public function testExecute(array $result, array $params): void
    {
        $position = new Vector($params['positionX'], $params['positionY']);
        $velocity = new Vector($params['velocityX'], $params['velocityY']);
        $newPosition = new Vector($result['newPositionX'], $result['newPositionY']);

        $moveInterface = $this->createMock(MoveInterface::class);

        $moveInterface
            ->method('getVectorVelocity')
            ->willReturn($velocity);

        $moveInterface
            ->method('getPosition')
            ->willReturn($position);

        $moveInterface
            ->expects($this->once())
            ->method('setPosition')
            ->with($newPosition);

        $moveCommand = new MoveCommand($moveInterface);
        $moveCommand->execute();
    }

    public function testGetPositionException(): void
    {
        $moveInterface = $this->createMock(MoveInterface::class);
        $moveInterface
            ->method('getPosition')
            ->willThrowException(new GetPropertySpaceObjectException('Свойство: position не типа Vector!'));

        $this->expectException(GetPropertySpaceObjectException::class);
        $this->expectExceptionMessage('Свойство: position не типа Vector!');

        $moveCommand = new MoveCommand($moveInterface);
        $moveCommand->execute();
    }

    public function testGetVectorVelocityException(): void
    {
        $moveInterface = $this->createMock(MoveInterface::class);
        $moveInterface
            ->method('getVectorVelocity')
            ->willThrowException(new GetPropertySpaceObjectException('Свойство: velocity не найдено!'));

        $this->expectException(GetPropertySpaceObjectException::class);
        $this->expectExceptionMessage('Свойство: velocity не найдено!');

        $moveCommand = new MoveCommand($moveInterface);
        $moveCommand->execute();
    }

    public function testSetPositionException(): void
    {
        $moveInterface = $this->createMock(MoveInterface::class);
        $moveInterface
            ->method('setPosition')
            ->willThrowException(
                new SetPropertySpaceObjectException('Ошибка установки нового значения свойства: position!')
            );

        $this->expectException(SetPropertySpaceObjectException::class);
        $this->expectExceptionMessage('Ошибка установки нового значения свойства: position!');

        $moveCommand = new MoveCommand($moveInterface);
        $moveCommand->execute();
    }
}