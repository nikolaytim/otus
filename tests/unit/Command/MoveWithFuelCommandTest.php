<?php

namespace UnitTests\Command;

use App\Command\CheckFuelCommand;
use App\Command\FuelCommand;
use App\Command\MoveCommand;
use App\Command\MoveWithFuelCommand;
use App\Exception\CommandException;
use PHPUnit\Framework\TestCase;

class MoveWithFuelCommandTest extends TestCase
{
    public function testExecute(): void
    {
        $checkFuelCommand = $this->createMock(CheckFuelCommand::class);
        $moveCommand = $this->createMock(MoveCommand::class);
        $fuelCommand = $this->createMock(FuelCommand::class);

        $checkFuelCommand
            ->expects($this->once())
            ->method('execute');

        $moveCommand
            ->expects($this->once())
            ->method('execute');

        $fuelCommand
            ->expects($this->once())
            ->method('execute');

        $moveWithFuelCommand = new MoveWithFuelCommand([
            $checkFuelCommand,
            $moveCommand,
            $fuelCommand
        ]);
        $moveWithFuelCommand->execute();
    }

    public function testExecuteException(): void
    {
        $checkFuelCommand = $this->createMock(CheckFuelCommand::class);
        $moveCommand = $this->createMock(MoveCommand::class);
        $fuelCommand = $this->createMock(FuelCommand::class);

        $checkFuelCommand
            ->method('execute')
            ->willThrowException(new CommandException('Недостаточно топлива!'));

        $moveCommand
            ->expects($this->never())
            ->method('execute');

        $fuelCommand
            ->expects($this->never())
            ->method('execute');

        $moveWithFuelCommand = new MoveWithFuelCommand([
            $checkFuelCommand,
            $moveCommand,
            $fuelCommand
        ]);
        $moveWithFuelCommand->execute();
    }
}