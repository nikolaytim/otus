<?php

namespace UnitTests\Command;

use App\Command\CommandInterface;
use App\Command\RepeatCommand;
use PHPUnit\Framework\TestCase;

class RepeatCommandTest extends TestCase
{
    public function testExecute(): void
    {
        $command = $this->createMock(CommandInterface::class);
        $command
            ->expects($this->once())
            ->method('execute');

        $repeatCommand = new RepeatCommand($command);
        $repeatCommand->execute();
    }
}