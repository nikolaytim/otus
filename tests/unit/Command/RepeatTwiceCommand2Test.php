<?php

namespace UnitTests\Command;

use App\Command\CommandInterface;
use App\Command\RepeatTwiceCommand2;
use PHPUnit\Framework\TestCase;

class RepeatTwiceCommand2Test extends TestCase
{
    public function testExecute(): void
    {
        $command = $this->createMock(CommandInterface::class);
        $command
            ->expects($this->once())
            ->method('execute');

        $repeatTwiceCommand2 = new RepeatTwiceCommand2($command);
        $repeatTwiceCommand2->execute();
    }
}
