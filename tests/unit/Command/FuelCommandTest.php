<?php

namespace UnitTests\Command;

use App\Adapter\FuelInterface;
use App\Command\FuelCommand;
use App\Exception\GetPropertySpaceObjectException;
use App\Exception\SetPropertySpaceObjectException;
use PHPUnit\Framework\TestCase;

class FuelCommandTest extends TestCase
{
    public function executeDataProvider(): array
    {
        return [
            'fuel_1' => [
                7,
                [
                    'quantityFuel' => 12,
                    'rateFuel' => 5,
                ],
            ],
            'fuel_2' => [
                -3,
                [
                    'quantityFuel' => 12,
                    'rateFuel' => 15,
                ],
            ],
        ];
    }

    /**
     * @dataProvider executeDataProvider
     */
    public function testExecute(int $result, array $params): void
    {
        $fuelInterface = $this->createMock(FuelInterface::class);

        $fuelInterface
            ->method('getQuantityFuel')
            ->willReturn($params['quantityFuel']);

        $fuelInterface
            ->method('getRateFuel')
            ->willReturn($params['rateFuel']);

        $fuelInterface
            ->expects($this->once())
            ->method('setQuantityFuel')
            ->with($result);

        $fuelCommand = new FuelCommand($fuelInterface);
        $fuelCommand->execute();
    }

    public function testGetQuantityFuelException(): void
    {
        $fuelInterface = $this->createMock(FuelInterface::class);
        $fuelInterface
            ->method('getQuantityFuel')
            ->willThrowException(new GetPropertySpaceObjectException('Свойство: quantityFuel не найдено!'));

        $this->expectException(GetPropertySpaceObjectException::class);
        $this->expectExceptionMessage('Свойство: quantityFuel не найдено!');

        $fuelCommand = new FuelCommand($fuelInterface);
        $fuelCommand->execute();
    }

    public function testGetRateFuelException(): void
    {
        $fuelInterface = $this->createMock(FuelInterface::class);
        $fuelInterface
            ->method('getRateFuel')
            ->willThrowException(new GetPropertySpaceObjectException('Свойство: rateFuel не найдено!'));

        $this->expectException(GetPropertySpaceObjectException::class);
        $this->expectExceptionMessage('Свойство: rateFuel не найдено!');

        $fuelCommand = new FuelCommand($fuelInterface);
        $fuelCommand->execute();
    }

    public function testSetQuantityFuelException(): void
    {
        $fuelInterface = $this->createMock(FuelInterface::class);
        $fuelInterface
            ->method('setQuantityFuel')
            ->willThrowException(
                new SetPropertySpaceObjectException('Ошибка установки нового значения свойства: quantityFuel!')
            );

        $this->expectException(SetPropertySpaceObjectException::class);
        $this->expectExceptionMessage('Ошибка установки нового значения свойства: quantityFuel!');

        $fuelCommand = new FuelCommand($fuelInterface);
        $fuelCommand->execute();
    }
}