<?php

namespace UnitTests\Command;

use App\Command\CommandInterface;
use App\Command\LogExceptionCommand;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class LogExceptionCommandTest extends TestCase
{
    public function testExecute(): void
    {
        $mockLoggerInterface = $this->createMock(LoggerInterface::class);
        $mockCommand = $this->createMock(CommandInterface::class);
        $mockException = $this->createMock(\Exception::class);

        $logExceptionCommand = $this->getMockBuilder(LogExceptionCommand::class)
            ->setConstructorArgs([$mockCommand, $mockException])
            ->onlyMethods(['getLogger'])
            ->getMock();

        $logExceptionCommand
            ->expects($this->once())
            ->method('getLogger')
            ->willReturn($mockLoggerInterface);

        $mockLoggerInterface
            ->expects($this->exactly(2))
            ->method('debug');

        $logExceptionCommand->execute();
    }
}