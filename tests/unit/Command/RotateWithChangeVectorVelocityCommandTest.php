<?php

namespace UnitTests\Command;

use App\Command\ChangeVectorVelocityCommand;
use App\Command\RotateCommand;
use App\Command\RotateWithChangeVectorVelocityCommand;
use App\Exception\CommandException;
use PHPUnit\Framework\TestCase;

class RotateWithChangeVectorVelocityCommandTest extends TestCase
{
    public function testExecute(): void
    {
        $rotateCommand = $this->createMock(RotateCommand::class);
        $changeVectorVelocityCommand = $this->createMock(ChangeVectorVelocityCommand::class);

        $rotateCommand
            ->expects($this->once())
            ->method('execute');

        $changeVectorVelocityCommand
            ->expects($this->once())
            ->method('execute');

        $rotateWithChangeVectorVelocityCommand = new RotateWithChangeVectorVelocityCommand([
            $rotateCommand,
            $changeVectorVelocityCommand,
        ]);
        $rotateWithChangeVectorVelocityCommand->execute();
    }

    public function testExecuteException(): void
    {
        $rotateCommand = $this->createMock(RotateCommand::class);
        $changeVectorVelocityCommand = $this->createMock(ChangeVectorVelocityCommand::class);

        $rotateCommand
            ->method('execute')
            ->willThrowException(new CommandException('Скорость не определена!'));

        $changeVectorVelocityCommand
            ->expects($this->once())
            ->method('execute');

        $rotateWithChangeVectorVelocityCommand = new RotateWithChangeVectorVelocityCommand([
            $rotateCommand,
            $changeVectorVelocityCommand,
        ]);
        $rotateWithChangeVectorVelocityCommand->execute();
    }
}