<?php

namespace UnitTests;

use App\Equation;
use PHPUnit\Framework\TestCase;
use \Exception;

class EquationTest extends TestCase
{
    const EPSILON = 0.00000000001;

    public function equationDataProvider(): array
    {
        return [
            'no_roots' => [
                [
                    'a' => 1.0,
                    'b' => 0.0,
                    'c' => 1.0,
                    'epsilon' => self::EPSILON
                ],
                []
            ],
            'one_root' => [
                [
                    'a' => 1.0,
                    'b' => 2.0,
                    'c' => 1.0,
                    'epsilon' => self::EPSILON
                ],
                [-1.0]
            ],
            'two_roots' => [
                [
                    'a' => 1.0,
                    'b' => 0.0,
                    'c' => -1.0,
                    'epsilon' => self::EPSILON
                ],
                [1.0, -1.0] // результат
            ],
        ];
    }

    public function equationExceptionDataProvider(): array
    {
        return [
            'a_lt_epsilon' => [
                [
                    'a' => 0.000000000001,
                    'b' => 1.0,
                    'c' => -1.0,
                    'epsilon' => self::EPSILON
                ],
                []
            ],
            'c_nan' => [
                [
                    'a' => 0.0, 
                    'b' => 1.0,
                    'c' => acos(2), // 'NaN'
                    'epsilon' => self::EPSILON
                ],
                []
            ],

        ];
    }

    /**
     * @dataProvider equationDataProvider
     */
    public function testEquation(array $params, array $expected): void
    {
        extract($params);
        $result = Equation::getInstance()->solve($a, $b, $c, $epsilon);
        $this->assertSame($expected, $result);
    }

    /**
     * @dataProvider equationExceptionDataProvider
     */
    public function testEquationException(array $params, array $expected): void
    {
        extract($params);
        $this->expectException(\Exception::class);
        $result = Equation::getInstance()->solve($a, $b, $c, $epsilon);
    }    
}